/* Author : Wu Yuansheng
 * Assignment : 5
 * Program Synopsis : Lazy simPL evaluator
 *
 */

import cs3212.simPL.*;
import java.util.*;

class EvaluatorLazy {
  
    public static Expression evaluateLazy(Expression e_with_lets) {
        return eval(EliminateLet.eliminateLet(e_with_lets));
    }

    public static Expression eval(Expression e) {

	// return values
  	if ((e instanceof IntConstant) ||
	    (e instanceof BoolConstant)||
	    (e instanceof Fun)||
	    (e instanceof RecFun))
	    return e;  	

	// return variables (occurs when stuck)
	else if (e instanceof Variable)
	    return e;

	// evaluate applications from left to right
	else if(e instanceof Application) {
	    Application a = (Application)e;
	    Expression newoperator = a.operator;
	    // Expression newoperator = eval(a.operator);
	    
	    Vector newoperands = new Vector();
	    Enumeration oe = a.operands.elements();
	    while (oe.hasMoreElements()) 
		newoperands.add((Expression)oe.nextElement());
		//newoperands.add(eval((Expression) oe.nextElement()));
	    
	    if(newoperator instanceof Fun) {
 		if (newoperands.size() == ((Fun) newoperator).formals.size()) {
		    return 
			eval(new Substitutions(((Fun) newoperator).formals,
					       newoperands)
			    .apply(((Fun) newoperator).body));
		} else return new Application(newoperator,newoperands); 
	    } else if (newoperator instanceof RecFun){
 		if (newoperands.size() == ((RecFun) newoperator).formals.size()) {
		    return 
			eval(new Substitution(((RecFun) newoperator).funVar,
					      newoperator)
			    .apply(new Substitutions(((RecFun) newoperator)
						     .formals,
						     newoperands)
				.apply(((RecFun) newoperator).body)));
		} else return new Application(newoperator,newoperands); 
	    } else {
		Expression new2operator = eval((Expression)newoperator);
		Vector new2operands = new Vector();
		Enumeration oe2 = newoperands.elements();
		while (oe2.hasMoreElements()) 
		    new2operands.add(eval((Expression)oe2.nextElement()));
		
		if (new2operator instanceof Fun) {
		    return eval(new Application(new2operator, new2operands));
		} else
		    return new Application(new2operator, new2operands); 
	    }
	}

	// evaluate primitive applications from left to right
        else if (e instanceof PrimitiveApplication){
        	PrimitiveApplication pa = (PrimitiveApplication) e;
        	Expression e1 = eval(pa.argument1);
        	Expression e2 = eval(pa.argument2);
		return
		    (pa.operator.equals("+"))
		    ?
		    (((e1 instanceof IntConstant) &&
		      (e2 instanceof IntConstant)   )
		     ?
		     (Expression) new IntConstant(((IntConstant)e1).value
						  +
						  ((IntConstant)e2).value)
		     : 
		     (Expression) new PrimitiveApplication("+",e1,e2)
		    )
		    :
		    (pa.operator.equals("-"))
		    ?
		    ((e1 instanceof IntConstant &&
		      e2 instanceof IntConstant)
		     ?
		     (Expression) new IntConstant(((IntConstant)e1).value
						  -
						  ((IntConstant)e2).value)
		     : 
		     (Expression) new PrimitiveApplication("-",e1,e2)
		    )
		    :
		    (pa.operator.equals("*"))
		    ?
		    ((e1 instanceof IntConstant &&
		      e2 instanceof IntConstant)
		     ?
		     (Expression) new IntConstant(((IntConstant)e1).value
						  *
						  ((IntConstant)e2).value)
		     : 
		     (Expression) new PrimitiveApplication("*",e1,e2)
		    )
		    :
		    (pa.operator.equals("/"))
		    ?
		    ((e1 instanceof IntConstant &&
		      e2 instanceof IntConstant &&
		      ((IntConstant)e2).value != 0)
		     ?
		     (Expression) new IntConstant((((IntConstant)e1).value)
						  /
						  (((IntConstant)e2).value))
		     : 
		     (Expression) new PrimitiveApplication("/",e1,e2)
		    )
		    :
		    (pa.operator.equals("="))
		    ?
		    ((e1 instanceof IntConstant &&
		      e2 instanceof IntConstant)
		     ?
		     (Expression) new BoolConstant(((IntConstant)e1).value
						   ==
						   ((IntConstant)e2).value)
		     : 
		     (Expression) new PrimitiveApplication("=",e1,e2)
		    )
		    :
		    (pa.operator.equals("<"))
		    ?
		    ((e1 instanceof IntConstant &&
		      e2 instanceof IntConstant)
		     ?
		     (Expression) new BoolConstant(((IntConstant)e1).value
						   <
						   ((IntConstant)e2).value)
		     : 
		     (Expression) new PrimitiveApplication("<",e1,e2)
		    )
		    :
		    (pa.operator.equals(">"))
		    ?
		    ((e1 instanceof IntConstant &&
		      e2 instanceof IntConstant)
		     ?
		     (Expression) new BoolConstant(((IntConstant)e1).value
						   >
						   ((IntConstant)e2).value)
		     : 
		     (Expression) new PrimitiveApplication(">",e1,e2)
		    )
		    :
		    (pa.operator.equals("|"))
		    ?
		    ((e1 instanceof BoolConstant &&
		      e2 instanceof BoolConstant)
		     ?
		     (Expression) new BoolConstant(((BoolConstant)e1).value
						   ||
						   ((BoolConstant)e2).value)
		     : 
		     (Expression) new PrimitiveApplication("|",e1,e2)
		    )
		    :
		    (pa.operator.equals("&"))
		    ?
		    ((e1 instanceof BoolConstant &&
		      e2 instanceof BoolConstant)
		     ?
		     (Expression) new BoolConstant(((BoolConstant)e1).value
						   &&
						   ((BoolConstant)e2).value)
		     : 
		     (Expression) new PrimitiveApplication("&",e1,e2)
		    )
		    :
		    (pa.operator.equals("\\"))
		    ?
		    ((e1 instanceof BoolConstant)
		     ?
		     (Expression) new BoolConstant(! ((BoolConstant)e1).value)
		     : 
		     (Expression) new PrimitiveApplication("\\",e1,e2)
		    )
		    :
		    (pa.operator.equals("~"))
		    ?
		    ((e1 instanceof IntConstant)
		     ?
		     (Expression) new IntConstant(- ((IntConstant)e1).value)
		     : 
		     (Expression) new PrimitiveApplication("~",e1,e2)
		    )
		    :
		    new PrimitiveApplication(pa.operator,e1,e2); }  

	// evaluate conditionals by evaluation the condition
	// and evaluating the appropriate part
	else if (e instanceof If){
	    If i = (If)e;
        	Expression c = i.condition;
        	Expression tp = i.thenPart; 
                Expression ep = i.elsePart;
                Expression ec = eval(c);
                if (ec instanceof BoolConstant) 
		    return 
			(((BoolConstant)ec).value)
			?
			eval(tp)
			:
			eval(ep);
                else
		    return new If(ec,tp,ep);
        }
  	else return e;
    }
}

